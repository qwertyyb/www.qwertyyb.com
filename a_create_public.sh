#!/bin/bash

directory="$CI_PROJECT_DIR/public"

if [ ! -d "$directory" ]; then
  mkdir "$directory"
  echo "Directory created: $directory"
else
  echo "Directory already exists: $directory"
  rm -rf "$directory"
  mkdir "$directory"
fi