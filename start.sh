#!/bin/bash

base=qwertyyb.github.io
repos=(Fire YPaste web-push "$base")

./a_create_public.sh

for repo in ${repos[@]}
do
echo "start download: $repo"
./b_remove_origin_dir.sh "$repo"
./c_download_repo.sh "$repo"
echo "end download: $repo"

done

./d_handle_base.sh $base

./e_commit.sh