#!/bin/bash

directory="$CI_PROJECT_DIR/public"

cd "$directory"

git clone --branch gh-pages --depth 1 --single-branch "https://github.com/qwertyyb/$1.git"

cd "$1"

ls -al

rm -rf ".git"