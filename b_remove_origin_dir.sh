#!/bin/bash

directory="$CI_PROJECT_DIR/public/$1"

if [ -d "$directory" ]; then
  rm -rf "$directory"
  echo "Directory removedd: $directory"
else
  echo "Directory not exists: $directory"
fi